## How to use with docker in staging

1. install portainer dan traefik
2. run `git clone https://gitlab.com/project/git/repo.git projectname`
3. run `sudo chown -R $USER:$USER projectname`
4. run `cd projectname`
5. run `docker run --rm -v $(pwd):/app composer install`
6. run `cp .env.example .env`
7. run `nano .env`
8. setup env sesuai kebutuhan khusus db host namakan dengan ${APP_NAME}-databse
9. simpan `ctrl+o`
10. exit `ctrl+x`
11. run `sudo chmod -R a+rw storage`
12. run `sudo chmod -R a+rw bootstrap/cache`
13. run `docker-compose up -d`
14. run `docker-compose exec --user root application php artisan key:generate`
15. run `docker-compose exec --user root application php artisan storage:link`
16. run `docker-compose exec --user root application php artisan config:cache`
17. run `docker-compose exec --user root application php artisan migrate`

## Note

1. run `docker-compose exec --user root application composer dumpautoload` when add new seeder
2. run `docker-compose exec --user root application php artisan migrate:refresh --path=/database/migrations/fileName.php` for refresh some table